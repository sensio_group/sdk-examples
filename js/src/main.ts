import { api } from "@sensio/api";
import snCid from "@sensio/op-sn-cid";
import { SnForWhat } from "@sensio/types";
import * as woss from "woss-test";
async function main(): Promise<void> {
  const ss: woss.R = {
    namE: "s",
  };
  console.log(ss);

  // const c_cid = new snCid({ data: new Uint8Array(7) });
  // await c_cid.run();
  // console.log("c_cid", c_cid.toString());

  const cid = await snCid({ data: new Uint8Array(7) });

  // console.log("cid", toString(cid), );
  console.log("cid", cid);

  const fw: SnForWhat = SnForWhat.FLOWCONTROL;
  console.log("fw", fw);

  const a = await api();
  // console.log(a);
  console.log((await a.query.operations.operationCount()).toNumber());
}
main()
  .then(() => console.log("DONE!!"))
  .catch(console.error);
